# FROM cypress/included:4.11.0
FROM cypress/browsers:node12.13.0-chrome80-ff73
COPY ./ /
RUN npm install
RUN ./node_modules/.bin/cypress install
EXPOSE 8000
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]