"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.testables = exports.slackRunner = void 0;
const webhook_1 = require("@slack/webhook");
const fs = require("fs");
const globby = require("globby");
//const AWS = require('aws-sdk');
const NextcloudClient = require('nextcloud-link');
const nodemailer = require('nodemailer');
const path = require("path");
const pino = require("pino");
const log = pino();

// AMAZON S3
/*const ID = 'AKIAJ35VRMC62N5ERYQQ';
const SECRET = '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';
const BUCKET_NAME = 'orbita';*/

// NEXTCLOUD
const NEXTCLOUD_USERNAME = "admin"
const NEXTCLOUD_PASSWORD = "interstell@r"
const NEXTCLOUD_URL = "https://orbita.temperini.digital"

exports.slackRunner = async (reportDir, videoDir, screenshotDir, htmlReportFile, onlyFailed = false) => {
    try {
        const reportHTMLUrl = await buildHTMLReportURL(reportDir, htmlReportFile);
        const videoAttachmentsSlack = await getVideoLinks(videoDir);
        const screenshotAttachmentsSlack = await getScreenshotLinks(screenshotDir);
        const reportStatistics = await getTestReportStatus(reportDir, htmlReportFile);

        if (onlyFailed && reportStatistics.status !== "failed") {
            return `onlyFailed flag set, test run status was ${reportStatistics.status}, so not sending message`;
        }
        else {
            const reports = await attachmentReports({
                reportStatistics,
                reportHTMLUrl
            });
            const SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/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';
            //const SLACK_WEBHOOK_URL = '';
            switch (reportStatistics.status) {
                case "failed": {
                    //Envia E-mail
                    sendEmail(reports, videoAttachmentsSlack, screenshotAttachmentsSlack);

                    const slackWebhookFailedUrl = process.env
                        .SLACK_WEBHOOK_FAILED_URL;
                    const slackWebhookUrls = slackWebhookFailedUrl
                        ? slackWebhookFailedUrl.split(",")
                        : SLACK_WEBHOOK_URL.split(",");
                    return await Promise.all(slackWebhookUrls.map(async (slackWebhookUrl) => {
                        const webhook = new webhook_1.IncomingWebhook(slackWebhookUrl);
                        const sendArguments = await webhookSendArgs({
                            argsWebhookSend: {},
                            messageAttachments: [reports],
                        });
                        log.info({ data: sendArguments }, "failing run");
                        try {
                            const result = await webhook.send(sendArguments);
                            log.info({ result, testStatus: reportStatistics }, "Slack message sent successfully");
                            return result;
                        }
                        catch (e) {
                            e.code
                                ? log.error({
                                    code: e.code,
                                    message: e.message,
                                    data: e.original.config.data,
                                }, "Failed to send slack message")
                                : log.error({ e }, "Unknown error occurred whilst sending slack message");
                            throw new Error("An error occurred whilst sending slack message");
                        }
                    }));
                }
                case "passed": {
                    const slackWebhookPassedUrl = process.env.SLACK_WEBHOOK_PASSED_URL;
                    const slackWebhookUrls = slackWebhookPassedUrl
                        ? slackWebhookPassedUrl.split(",")
                        : SLACK_WEBHOOK_URL.split(",");
                    return await Promise.all(slackWebhookUrls.map(async (slackWebhookUrl) => {
                        const webhook = new webhook_1.IncomingWebhook(slackWebhookUrl);
                        const sendArguments = await webhookSendArgs({
                            argsWebhookSend: {},
                            messageAttachments: [reports],
                        });
                        log.info({ data: sendArguments }, "passing run");
                        try {
                            const result = await webhook.send(sendArguments);
                            log.info({ result, testStatus: reportStatistics }, "Slack message sent successfully");
                            return result;
                        }
                        catch (e) {
                            e.code
                                ? log.error({
                                    code: e.code,
                                    message: e.message,
                                    data: e.original.config.data,
                                }, "Failed to send slack message")
                                : log.error({ e }, "Unknown error occurred whilst sending slack message");
                            throw new Error("An error occurred whilst sending slack message");
                        }
                    }));
                }
                default: {
                    const slackWebhookErrorUrl = process.env
                        .SLACK_WEBHOOK_ERROR_URL;
                    const slackWebhookUrls = slackWebhookErrorUrl
                        ? slackWebhookErrorUrl.split(",")
                        : SLACK_WEBHOOK_URL.split(",");
                    return await Promise.all(slackWebhookUrls.map(async (slackWebhookUrl) => {
                        const webhook = new webhook_1.IncomingWebhook(slackWebhookUrl);
                        const sendArguments = await webhookSendArgs({
                            argsWebhookSend: {},
                            messageAttachments: [reports],
                        });
                        log.debug({ data: sendArguments }, "erroring run");
                        try {
                            const result = await webhook.send(sendArguments);
                            log.info({ result, testStatus: reportStatistics }, "Slack message sent successfully");
                            return result;
                        }
                        catch (e) {
                            e.code
                                ? log.error({
                                    code: e.code,
                                    message: e.message,
                                    data: e.original.config.data,
                                }, "Failed to send slack message")
                                : log.error({ e }, "Unknown error occurred whilst sending slack message");
                            throw new Error("An error occurred whilst sending slack message");
                        }
                    }));
                }
            }
        }
    }
    catch (e) {
        throw new Error(e);
    }
};
const webhookSendArgs = async ({ argsWebhookSend, messageAttachments, }) => {
    argsWebhookSend = {
        attachments: messageAttachments,
        unfurl_links: false,
        unfurl_media: false,
    };
    return argsWebhookSend;
};
const attachmentReports = async ({ reportStatistics, reportHTMLUrl }) => {
    switch (reportStatistics.status) {
        case "passed": {
            return {
                color: "#36a64f",
                fallback: `Relatório disponível em ${reportHTMLUrl}`,
                text: `Testes com sucesso:  ${reportStatistics.totalPasses}`,
                actions: [
                    {
                        type: "button",
                        text: "Acessar relatório",
                        url: `${reportHTMLUrl}`,
                        style: "primary",
                    }
                ],
            };
        }
        case "failed": {
            return {
                color: "#ff0000",
                fallback: `Relatório disponível em ${reportHTMLUrl}`,
                title: `Testes com falha: ${reportStatistics.totalFailures}`,
                text: `Total de testes: ${reportStatistics.totalTests}\nSucesso:  ${reportStatistics.totalPasses} `,
                textEmail: `<td style="border-top-width: 0; border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; border-top-color: #e9ecef; border-top-style: solid; margin: 0; padding: 12px;" align="left" valign="top">Total de testes: ${reportStatistics.totalTests}</td>
                <td style="border-top-width: 0; border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; border-top-color: #e9ecef; border-top-style: solid; margin: 0; padding: 12px;" class="text-right" align="right" valign="top">Sucesso:  ${reportStatistics.totalPasses}</td>`,
                actions: [
                    {
                        type: "button",
                        text: "Acessar relatório",
                        url: `${reportHTMLUrl}`,
                        style: "primary",
                    }
                ],
            };
        }
        /*case "error": {
            return {
                color: "#ff0000",
                text: `Total Passed:  ${reportStatistics.totalPasses} `,
                actions: [
                    {
                        type: "button",
                        text: "Build Logs",
                        url: `${ciEnvVars.CI_BUILD_URL}`,
                        style: "danger",
                    },
                ],
            };
        }*/
        default: {
            return {};
        }
    }
};
const attachmentsVideoAndScreenshots = async ({ status, videoAttachmentsSlack, screenshotAttachmentsSlack, }) => {
    switch (status) {
        case "passed": {
            return {
                text: `${videoAttachmentsSlack}${screenshotAttachmentsSlack}`,
                color: "#36a64f",
            };
        }
        case "failed": {
            return {
                text: `${videoAttachmentsSlack}${screenshotAttachmentsSlack}`,
                color: "#ff0000",
            };
        }
        default: {
            return {};
        }
    }
};
const getTestReportStatus = async (reportDir, reportFile) => {
    var reportJSONFilename = reportFile.split('\\').pop().split('/').pop();
    const rawdata = fs.readFileSync(reportDir + '/' + reportJSONFilename+'.json');
    const parsedData = JSON.parse(rawdata.toString());
    const reportStats = parsedData.stats;
    const totalSuites = reportStats.suites;
    const totalTests = reportStats.tests;
    const totalPasses = reportStats.passes;
    const totalFailures = reportStats.failures;
    const totalDuration = reportStats.duration;
    if (totalTests === undefined || totalTests === 0) {
        reportStats.status = "error";
    }
    else if (totalFailures > 0 || totalPasses === 0) {
        reportStats.status = "failed";
    }
    else if (totalFailures === 0) {
        reportStats.status = "passed";
    }
    return {
        totalSuites,
        totalTests,
        totalPasses,
        totalFailures,
        totalDuration,
        reportJSONFilename,
        status: reportStats.status,
    };
};
const getVideoLinks = async (videosDir) => {
    const videos = await globby(path.resolve(process.cwd(), videosDir), {
        expandDirectories: {
            files: ["*"],
            extensions: ["mp4"],
        },
    });
    if (videos.length === 0) {
        return "";
    }
    else {
        var arrVid = [];
        videos.forEach(function(videoObject) {
            const trimmedVideoFilename = path.basename(videoObject);
            //var fileUrl = uploadFile(videosDir + '/' + trimmedVideoFilename);
            arrVid.push({ filename: trimmedVideoFilename, path: videosDir + '/' + trimmedVideoFilename});
        });
        return arrVid;
    }
};
const getScreenshotLinks = async (screenshotDir) => {
    const screenshots = await globby(path.resolve(process.cwd(), screenshotDir), {
        expandDirectories: {
            files: ["*"],
            extensions: ["png"],
        },
    });
    if (screenshots.length === 0) {
        return "";
    }
    else {
        var arrPic = [];
        screenshots.forEach(function(screenshotObject) {
            const trimmedVideoFilename = path.basename(screenshotObject);
            //var fileUrl = uploadFile(screenshotObject);
            arrPic.push({ filename: trimmedVideoFilename, path: screenshotObject });
        });
        return arrPic;
    }
};
const buildHTMLReportURL = async (reportDir, htmlReportFile) => {
    var reportHTMLFilename = htmlReportFile.split('\\').pop().split('/').pop();
    let date_ob = new Date();

    // current date
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();
    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    var fileId = year + "-" + month + "-" + date + "-" + hours + "-"+minutes;
    var fileUrl = uploadFile(reportDir + "/" + reportHTMLFilename+'.html', reportHTMLFilename+'_'+fileId+'.html');
    return fileUrl;
};
// Faz upload dos arquivos para o nextcloud
const uploadFile = async (filepath, destination) => {
    try {
        const client = new NextcloudClient({
          url: NEXTCLOUD_URL,
          username: NEXTCLOUD_USERNAME,
          password: NEXTCLOUD_PASSWORD
        });
    
        await client.checkConnectivity();
        await client.put('/uploads/'+destination, fs.readFileSync(filepath));
        var link = await client.shares.add('/uploads/'+destination, 3);
        return link.url+'/download';
      } catch (error) {
        console.log(error);
      }
}
/*const uploadFile = (fileName) => {
    const s3 = new AWS.S3({
        accessKeyId: ID,
        secretAccessKey: SECRET
    });

    // Read content from the file
    var url = '';
    const fileContent = fs.readFileSync(fileName);

    // Setting up S3 upload parameters
    const params = {
        Bucket: BUCKET_NAME,
        Key: path.basename(fileName), // File name you want to save as in S3
        Body: fileContent,
        ACL: 'public-read'
    };

    // Uploading files to the bucket
    var s3upload = s3.upload(params).promise();

    return s3upload.then(function(data) {
        return data.Location;
    })
    .catch(function(err) {
        throw(err);
    });
};*/
const sendEmail = (reports, videoAttachments, screenshotAttachments) => {
    var html = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
       <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <style type="text/css">
             .ExternalClass{width:100%}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:150%}a{text-decoration:none}@media screen and (max-width: 600px){table.row th.col-lg-1,table.row th.col-lg-2,table.row th.col-lg-3,table.row th.col-lg-4,table.row th.col-lg-5,table.row th.col-lg-6,table.row th.col-lg-7,table.row th.col-lg-8,table.row th.col-lg-9,table.row th.col-lg-10,table.row th.col-lg-11,table.row th.col-lg-12{display:block;width:100% !important}.d-mobile{display:block !important}.d-desktop{display:none !important}.w-lg-25{width:auto !important}.w-lg-25>tbody>tr>td{width:auto !important}.w-lg-50{width:auto !important}.w-lg-50>tbody>tr>td{width:auto !important}.w-lg-75{width:auto !important}.w-lg-75>tbody>tr>td{width:auto !important}.w-lg-100{width:auto !important}.w-lg-100>tbody>tr>td{width:auto !important}.w-lg-auto{width:auto !important}.w-lg-auto>tbody>tr>td{width:auto !important}.w-25{width:25% !important}.w-25>tbody>tr>td{width:25% !important}.w-50{width:50% !important}.w-50>tbody>tr>td{width:50% !important}.w-75{width:75% !important}.w-75>tbody>tr>td{width:75% !important}.w-100{width:100% !important}.w-100>tbody>tr>td{width:100% !important}.w-auto{width:auto !important}.w-auto>tbody>tr>td{width:auto !important}.p-lg-0>tbody>tr>td{padding:0 !important}.pt-lg-0>tbody>tr>td,.py-lg-0>tbody>tr>td{padding-top:0 !important}.pr-lg-0>tbody>tr>td,.px-lg-0>tbody>tr>td{padding-right:0 !important}.pb-lg-0>tbody>tr>td,.py-lg-0>tbody>tr>td{padding-bottom:0 !important}.pl-lg-0>tbody>tr>td,.px-lg-0>tbody>tr>td{padding-left:0 !important}.p-lg-1>tbody>tr>td{padding:0 !important}.pt-lg-1>tbody>tr>td,.py-lg-1>tbody>tr>td{padding-top:0 !important}.pr-lg-1>tbody>tr>td,.px-lg-1>tbody>tr>td{padding-right:0 !important}.pb-lg-1>tbody>tr>td,.py-lg-1>tbody>tr>td{padding-bottom:0 !important}.pl-lg-1>tbody>tr>td,.px-lg-1>tbody>tr>td{padding-left:0 !important}.p-lg-2>tbody>tr>td{padding:0 !important}.pt-lg-2>tbody>tr>td,.py-lg-2>tbody>tr>td{padding-top:0 !important}.pr-lg-2>tbody>tr>td,.px-lg-2>tbody>tr>td{padding-right:0 !important}.pb-lg-2>tbody>tr>td,.py-lg-2>tbody>tr>td{padding-bottom:0 !important}.pl-lg-2>tbody>tr>td,.px-lg-2>tbody>tr>td{padding-left:0 !important}.p-lg-3>tbody>tr>td{padding:0 !important}.pt-lg-3>tbody>tr>td,.py-lg-3>tbody>tr>td{padding-top:0 !important}.pr-lg-3>tbody>tr>td,.px-lg-3>tbody>tr>td{padding-right:0 !important}.pb-lg-3>tbody>tr>td,.py-lg-3>tbody>tr>td{padding-bottom:0 !important}.pl-lg-3>tbody>tr>td,.px-lg-3>tbody>tr>td{padding-left:0 !important}.p-lg-4>tbody>tr>td{padding:0 !important}.pt-lg-4>tbody>tr>td,.py-lg-4>tbody>tr>td{padding-top:0 !important}.pr-lg-4>tbody>tr>td,.px-lg-4>tbody>tr>td{padding-right:0 !important}.pb-lg-4>tbody>tr>td,.py-lg-4>tbody>tr>td{padding-bottom:0 !important}.pl-lg-4>tbody>tr>td,.px-lg-4>tbody>tr>td{padding-left:0 !important}.p-lg-5>tbody>tr>td{padding:0 !important}.pt-lg-5>tbody>tr>td,.py-lg-5>tbody>tr>td{padding-top:0 !important}.pr-lg-5>tbody>tr>td,.px-lg-5>tbody>tr>td{padding-right:0 !important}.pb-lg-5>tbody>tr>td,.py-lg-5>tbody>tr>td{padding-bottom:0 !important}.pl-lg-5>tbody>tr>td,.px-lg-5>tbody>tr>td{padding-left:0 !important}.p-0>tbody>tr>td{padding:0 !important}.pt-0>tbody>tr>td,.py-0>tbody>tr>td{padding-top:0 !important}.pr-0>tbody>tr>td,.px-0>tbody>tr>td{padding-right:0 !important}.pb-0>tbody>tr>td,.py-0>tbody>tr>td{padding-bottom:0 !important}.pl-0>tbody>tr>td,.px-0>tbody>tr>td{padding-left:0 !important}.p-1>tbody>tr>td{padding:4px !important}.pt-1>tbody>tr>td,.py-1>tbody>tr>td{padding-top:4px !important}.pr-1>tbody>tr>td,.px-1>tbody>tr>td{padding-right:4px !important}.pb-1>tbody>tr>td,.py-1>tbody>tr>td{padding-bottom:4px !important}.pl-1>tbody>tr>td,.px-1>tbody>tr>td{padding-left:4px !important}.p-2>tbody>tr>td{padding:8px !important}.pt-2>tbody>tr>td,.py-2>tbody>tr>td{padding-top:8px !important}.pr-2>tbody>tr>td,.px-2>tbody>tr>td{padding-right:8px !important}.pb-2>tbody>tr>td,.py-2>tbody>tr>td{padding-bottom:8px !important}.pl-2>tbody>tr>td,.px-2>tbody>tr>td{padding-left:8px !important}.p-3>tbody>tr>td{padding:16px !important}.pt-3>tbody>tr>td,.py-3>tbody>tr>td{padding-top:16px !important}.pr-3>tbody>tr>td,.px-3>tbody>tr>td{padding-right:16px !important}.pb-3>tbody>tr>td,.py-3>tbody>tr>td{padding-bottom:16px !important}.pl-3>tbody>tr>td,.px-3>tbody>tr>td{padding-left:16px !important}.p-4>tbody>tr>td{padding:24px !important}.pt-4>tbody>tr>td,.py-4>tbody>tr>td{padding-top:24px !important}.pr-4>tbody>tr>td,.px-4>tbody>tr>td{padding-right:24px !important}.pb-4>tbody>tr>td,.py-4>tbody>tr>td{padding-bottom:24px !important}.pl-4>tbody>tr>td,.px-4>tbody>tr>td{padding-left:24px !important}.p-5>tbody>tr>td{padding:48px !important}.pt-5>tbody>tr>td,.py-5>tbody>tr>td{padding-top:48px !important}.pr-5>tbody>tr>td,.px-5>tbody>tr>td{padding-right:48px !important}.pb-5>tbody>tr>td,.py-5>tbody>tr>td{padding-bottom:48px !important}.pl-5>tbody>tr>td,.px-5>tbody>tr>td{padding-left:48px !important}.s-lg-1>tbody>tr>td,.s-lg-2>tbody>tr>td,.s-lg-3>tbody>tr>td,.s-lg-4>tbody>tr>td,.s-lg-5>tbody>tr>td{font-size:0 !important;line-height:0 !important;height:0 !important}.s-0>tbody>tr>td{font-size:0 !important;line-height:0 !important;height:0 !important}.s-1>tbody>tr>td{font-size:4px !important;line-height:4px !important;height:4px !important}.s-2>tbody>tr>td{font-size:8px !important;line-height:8px !important;height:8px !important}.s-3>tbody>tr>td{font-size:16px !important;line-height:16px !important;height:16px !important}.s-4>tbody>tr>td{font-size:24px !important;line-height:24px !important;height:24px !important}.s-5>tbody>tr>td{font-size:48px !important;line-height:48px !important;height:48px !important}}@media yahoo{.d-mobile{display:none !important}.d-desktop{display:block !important}.w-lg-25{width:25% !important}.w-lg-25>tbody>tr>td{width:25% !important}.w-lg-50{width:50% !important}.w-lg-50>tbody>tr>td{width:50% !important}.w-lg-75{width:75% !important}.w-lg-75>tbody>tr>td{width:75% !important}.w-lg-100{width:100% !important}.w-lg-100>tbody>tr>td{width:100% !important}.w-lg-auto{width:auto !important}.w-lg-auto>tbody>tr>td{width:auto !important}.p-lg-0>tbody>tr>td{padding:0 !important}.pt-lg-0>tbody>tr>td,.py-lg-0>tbody>tr>td{padding-top:0 !important}.pr-lg-0>tbody>tr>td,.px-lg-0>tbody>tr>td{padding-right:0 !important}.pb-lg-0>tbody>tr>td,.py-lg-0>tbody>tr>td{padding-bottom:0 !important}.pl-lg-0>tbody>tr>td,.px-lg-0>tbody>tr>td{padding-left:0 !important}.p-lg-1>tbody>tr>td{padding:4px !important}.pt-lg-1>tbody>tr>td,.py-lg-1>tbody>tr>td{padding-top:4px !important}.pr-lg-1>tbody>tr>td,.px-lg-1>tbody>tr>td{padding-right:4px !important}.pb-lg-1>tbody>tr>td,.py-lg-1>tbody>tr>td{padding-bottom:4px !important}.pl-lg-1>tbody>tr>td,.px-lg-1>tbody>tr>td{padding-left:4px !important}.p-lg-2>tbody>tr>td{padding:8px !important}.pt-lg-2>tbody>tr>td,.py-lg-2>tbody>tr>td{padding-top:8px !important}.pr-lg-2>tbody>tr>td,.px-lg-2>tbody>tr>td{padding-right:8px !important}.pb-lg-2>tbody>tr>td,.py-lg-2>tbody>tr>td{padding-bottom:8px !important}.pl-lg-2>tbody>tr>td,.px-lg-2>tbody>tr>td{padding-left:8px !important}.p-lg-3>tbody>tr>td{padding:16px !important}.pt-lg-3>tbody>tr>td,.py-lg-3>tbody>tr>td{padding-top:16px !important}.pr-lg-3>tbody>tr>td,.px-lg-3>tbody>tr>td{padding-right:16px !important}.pb-lg-3>tbody>tr>td,.py-lg-3>tbody>tr>td{padding-bottom:16px !important}.pl-lg-3>tbody>tr>td,.px-lg-3>tbody>tr>td{padding-left:16px !important}.p-lg-4>tbody>tr>td{padding:24px !important}.pt-lg-4>tbody>tr>td,.py-lg-4>tbody>tr>td{padding-top:24px !important}.pr-lg-4>tbody>tr>td,.px-lg-4>tbody>tr>td{padding-right:24px !important}.pb-lg-4>tbody>tr>td,.py-lg-4>tbody>tr>td{padding-bottom:24px !important}.pl-lg-4>tbody>tr>td,.px-lg-4>tbody>tr>td{padding-left:24px !important}.p-lg-5>tbody>tr>td{padding:48px !important}.pt-lg-5>tbody>tr>td,.py-lg-5>tbody>tr>td{padding-top:48px !important}.pr-lg-5>tbody>tr>td,.px-lg-5>tbody>tr>td{padding-right:48px !important}.pb-lg-5>tbody>tr>td,.py-lg-5>tbody>tr>td{padding-bottom:48px !important}.pl-lg-5>tbody>tr>td,.px-lg-5>tbody>tr>td{padding-left:48px !important}.s-lg-0>tbody>tr>td{font-size:0 !important;line-height:0 !important;height:0 !important}.s-lg-1>tbody>tr>td{font-size:4px !important;line-height:4px !important;height:4px !important}.s-lg-2>tbody>tr>td{font-size:8px !important;line-height:8px !important;height:8px !important}.s-lg-3>tbody>tr>td{font-size:16px !important;line-height:16px !important;height:16px !important}.s-lg-4>tbody>tr>td{font-size:24px !important;line-height:24px !important;height:24px !important}.s-lg-5>tbody>tr>td{font-size:48px !important;line-height:48px !important;height:48px !important}}
          </style>
       </head>
       <!-- Edit the code below this line -->
       <body style="outline: 0; width: 100%; min-width: 100%; height: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 24px; font-weight: normal; font-size: 16px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; margin: 0; padding: 0; border: 0;">
          <div class="preview" style="display: none; max-height: 0px; overflow: hidden;">
             Thank you for riding with Lyft!                                                                     
          </div>
          <table valign="top" class="bg-light body" style="outline: 0; width: 100%; min-width: 100%; height: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 24px; font-weight: normal; font-size: 16px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; margin: 0; padding: 0; border: 0;" bgcolor="#f8f9fa">
             <tbody>
                <tr>
                   <td valign="top" style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; margin: 0;" align="left" bgcolor="#f8f9fa">
                      <table class="container" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%;">
                         <tbody>
                            <tr>
                               <td align="center" style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; margin: 0; padding: 0 16px;">
                                  <!--[if (gte mso 9)|(IE)]>
                                  <table align="center">
                                     <tbody>
                                        <tr>
                                           <td width="600">
                                              <![endif]-->
                                              <table align="center" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%; max-width: 600px; margin: 0 auto;">
                                                 <tbody>
                                                    <tr>
                                                       <td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; margin: 0;" align="left">
                                                          <table class="card " border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: separate !important; border-radius: 4px; width: 100%; overflow: hidden; border: 1px solid #dee2e6;" bgcolor="#ffffff">
                                                             <tbody>
                                                                <tr>
                                                                   <td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; width: 100%; margin: 0;" align="left">
                                                                      <div style="border-top-width: 5px; border-top-color: ${reports.color}; border-top-style: solid;">
                                                                         <table class="card-body" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%;">
                                                                            <tbody>
                                                                               <tr>
                                                                                  <td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; width: 100%; margin: 0; padding: 20px;" align="left">
                                                                                     <div>
                                                                                        <h4 class="text-center" style="margin-top: 0; margin-bottom: 0; font-weight: 500; color: inherit; vertical-align: baseline; font-size: 24px; line-height: 28.8px;" align="center">${reports.title}</h4>
                                                                                        <table class="table" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%; max-width: 100%;" bgcolor="#ffffff">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                ${reports.textEmail}
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                        <table class="table" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%; max-width: 100%;" bgcolor="#ffffff">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; border-top-width: 1px; border-top-color: #e9ecef; border-top-style: solid; margin: 0; padding: 12px;" align="left" valign="top">
                                                                                                    <table class="s-2 w-100" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                                       <tbody>
                                                                                                          <tr>
                                                                                                             <td height="8" style="border-spacing: 0px; border-collapse: collapse; line-height: 8px; font-size: 8px; width: 100%; height: 8px; margin: 0;" align="left">
                                                                                                                 
                                                                                                             </td>
                                                                                                          </tr>
                                                                                                       </tbody>
                                                                                                    </table>
                                                                                                    <table class="btn btn-primary btn-lg mx-auto " align="center" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: separate !important; border-radius: 4px; margin: 0 auto;">
                                                                                                       <tbody>
                                                                                                          <tr>
                                                                                                             <td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; border-radius: 4px; margin: 0;" align="center" bgcolor="#007bff">
                                                                                                                <a href="${reports.actions[0].url}" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; text-decoration: none; border-radius: 4.8px; line-height: 30px; display: inline-block; font-weight: normal; white-space: nowrap; background-color: #007bff; color: #ffffff; padding: 8px 16px; border: 1px solid #007bff;">Baixar relatório</a>
                                                                                                             </td>
                                                                                                          </tr>
                                                                                                       </tbody>
                                                                                                    </table>
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </div>
                                                                                  </td>
                                                                               </tr>
                                                                            </tbody>
                                                                         </table>
                                                                      </div>
                                                                   </td>
                                                                </tr>
                                                             </tbody>
                                                          </table>
                                                          <table class="s-4 w-100" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                             <tbody>
                                                                <tr>
                                                                   <td height="24" style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 24px; width: 100%; height: 24px; margin: 0;" align="left">
                                                                       
                                                                   </td>
                                                                </tr>
                                                             </tbody>
                                                          </table>
                                                       </td>
                                                    </tr>
                                                 </tbody>
                                              </table>
                                              <!--[if (gte mso 9)|(IE)]>
                                           </td>
                                        </tr>
                                     </tbody>
                                  </table>
                                  <![endif]-->
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
             </tbody>
          </table>
       </body>
    </html>`;

    let transport = nodemailer.createTransport({
        host: "smtp.umbler.com",
        port: 587,
        secure: false,
        auth: {
            user: 'lucas@jointdev.com.br',
            pass: 'Interstellar2017'
        },
    });

    var attachments = [];
    if(videoAttachments.length) {
        videoAttachments.forEach(function(video) {
            attachments.push(video);
        });
    }

    if(screenshotAttachments.length) {
        screenshotAttachments.forEach(function(pic) {
            attachments.push(pic);
        });
    }

    var maillist = [
        'lucas.snta@gmail.com',
        'afantoni@maiscampus.com.br',
        'boliveira@grupoa.com.br'
    ];

    const messageObj = {
        from: 'lucas@jointdev.com.br',
        to: maillist, 
        subject: 'Relatório de testes',
        html: html,
        attachments: attachments
    };

    transport.sendMail(messageObj);
};
exports.testables = {
    buildHTMLReportURL,
    getScreenshotLinks,
    getVideoLinks,
    webhookSendArgs,
    attachmentReports,
    attachmentsVideoAndScreenshots,
    getTestReportStatus
};
