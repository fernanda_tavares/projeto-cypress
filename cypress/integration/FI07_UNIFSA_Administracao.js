const Leite = require('leite');
const leite = new Leite

describe('Acessar o site da IES UNIFSA.', () => {
  it('Visitar IES', () => {
    cy
      .visit("https://ead.unifsa.com.br/inscricao?utm_source=teste&utm_medium=campus")
      .clearCookies()
      .clearLocalStorage()
  });
  describe('Popular os campos da etapa', () => {
    it('Inserir primeiro nome', () => {
      cy
        .get('input[name="firstname"]')
        .type(leite.pessoa.primeiroNome())
    });
    it('Inserir sobrenome', () => {
      cy
        .get('input[name="lastname"]')
        .type(leite.pessoa.sobrenome() + " TESTE")
    });
    it('Inserir email', () => {
      cy
        .server()
        .route('POST', '/emailcheck/v1/*').as('emailCheck')
      cy
        .get('input[name="email"]')
        .type('teste' + leite.pessoa.email())
        .wait('@emailCheck').its('status').should('eq', 200)
    });
    it('Inserir telefone', () => {
      cy
        .server()
        .route('POST', '/emailcheck/v1/*').as('emailCheck')
      cy
        .get('input[name="phone"]')
        .type(`11 9${Math.floor((Math.random() * 1000000000)+100000000)}`)
        .wait('@emailCheck').its('status').should('eq', 200)
    });
    it('Autorizar uso do Whatsapp', () => {
      cy
        .get('input[name="podemos_usar_o_whatsapp_para_falar_sobre_sua_inscricao_e_matricula_"]')
        .check()
    })
    it('Enviar dados', () => {
      cy
        .get('input[type="submit"]')
        .first()
        .click()
    });
  });
});

describe('Inscrição no curso', () => {
  describe('Escolha do curso', () => {
    it('Forma de ingresso', () => {
      cy
        .server()
        .route('GET', '/v1/subscription/courses*').as('getCourses')
      cy
        .get('.v-slide-group__content > span') //forma de curso
        .first()
        .click()
        // .wait('@getCourses').its('status').should('eq', 200)
    });
    it('Selecionar o curso Administração', () => {
      cy
        .server()
        .route('GET', '/v1/subscription/campuses*').as('getCampuses')
      cy
        .get('.v-select__slot > input[id="selCoursePickerCourse"]')
        .click()
        .get('.v-select-list > .v-list-item')
        .contains('Administração') //curso da lista fornecida
        .click()
        .wait('@getCampuses').its('status').should('eq', 200)
    });
    it('Selecionar o local/campus', () => {
      cy
        .get('.v-select__slot > input[id="selCoursePickerCampus"]')
        .click()
        .get('div[role="listbox"]')
        .eq(1)
        .children()
        .first() //local
        .click()
    })
    it('Continuar inscrição', () => {
      cy
        .server()
        .route('POST', '/v1/subscription/institutions/8/deals').as('postInscrition')
      cy
        .get('button[id="btnContinueCRS"]')
        .click()
        .wait('@postInscrition').its('status').should('eq', 200)
    });
  })
});

describe('Confirmação de dados', () => {
  it('Inserir o CPF', () => {
    cy
      .server()
      .route('GET', '/v1/subscription/users/*').as('getDocumentStudent')
    cy
      .get('input[id="txtPersonalDataCPF"]')
      .type(leite.pessoa.cpf({
        formatado: true
      }))
      .wait('@getDocumentStudent').its('status').should('eq', 404)
  });
  it('Confirmar inscrição', () => {
    cy
      .server()
      .route('PUT', '/v1/subscription/users/signup/*').as('putSignup')
      .route('POST', '/v1/subscription/subscriptions').as('continueToTest')
    cy
      .get('button[id="btnContinuePSN"]')
      .click()
      .wait('@putSignup').its('status').should('eq', 200)
      .wait('@continueToTest').its('status').should('eq', 200)
  })
});