var http = require("http");
const { exec } = require('child_process');
http.createServer(function(request, response) {
    exec('./run-test', (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end(`stdout: ${stdout}`);
        response.end(`stderr: ${stderr}`);
    });
}).listen(8000, '0.0.0.0');
console.log('Servidor executando em http://0.0.0.0:8000/');